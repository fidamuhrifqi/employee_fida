package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOAgama;
import com.project.employee.models.Agama;
import com.project.employee.repositories.AgamaRepository;

@RestController
@RequestMapping("/api/agama")
public class AgamaController {

	@Autowired
    AgamaRepository agamaRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a Agama
    @DeleteMapping("/delete/{id}")
    public Map<String, Object> deleteAgama(@PathVariable(value = "id") Long agamaId) {
    	
    	Agama agama = agamaRepository.findById(agamaId).get();
    	Map<String, Object> result = new HashMap<String,Object>();
    	
    	agamaRepository.delete(agama);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

    	return result;	
    }
    
    // Parsing from Entity to DTO
    // Get Agama
    @GetMapping("/readAll")
    public Map<String, Object> getReadAllAgama(){
    	
		List<Agama> listAgamaEntity = agamaRepository.findAll();
		List<DTOAgama> listDTOAgama = new ArrayList<DTOAgama>();
		Map<String, Object> result = new HashMap<String,Object>();
		
		for (Agama agamaEntity : listAgamaEntity) {
			DTOAgama agamaDTO = new DTOAgama();
			
			agamaDTO = modelMapper.map(agamaEntity, DTOAgama.class);
			
			listDTOAgama.add(agamaDTO);
		}
		
        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOAgama);

    	return result;	
    }
    
    // Create a new  Agama
    @PostMapping("/create")
    public Map<String, Object> createDTOAgama(@Valid @RequestBody DTOAgama agamaDTO){
       
    	Map<String, Object> result = new HashMap<String,Object>();
        Agama agamaEntity = new Agama();

        agamaEntity = modelMapper.map(agamaDTO, Agama.class);
        
        agamaRepository.save(agamaEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", agamaDTO);

        return result;
    }
    
    // Get a Single Note
    @GetMapping("/getsingle/{id}")
    public Map<String, Object> getReadOneDTOAgama(@PathVariable(value = "id") Long agamaId){
		
    	Map<String, Object> result = new HashMap<String,Object>();
    	Agama agamaEntity = agamaRepository.findById(agamaId).get();
    	DTOAgama agamaDTO = new DTOAgama();
    	
    	agamaDTO = modelMapper.map(agamaEntity, DTOAgama.class);
    	
    	result.put("Status", 200);
        result.put("message", "Get Single Data Success");
        result.put("Data", agamaDTO);

        return result;
    }
    
    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String, Object> updateDTOAgama(@PathVariable(value = "id") Long agamaId,
            @Valid @RequestBody DTOAgama agamaDTO) {
			
    	Map<String, Object> result = new HashMap<String,Object>();
    	Agama agamaEntity = agamaRepository.findById(agamaId).get();

    	agamaEntity = modelMapper.map(agamaDTO, Agama.class);

        agamaRepository.save(agamaEntity);
    	
    	result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", agamaDTO);
        
    	return result;
    }
}
