package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOKemampuan;
import com.project.employee.models.Kemampuan;
import com.project.employee.repositories.KemampuanRepository;

@RestController
@RequestMapping("/api/kemampuan")
public class KemampuanController {
	
	@Autowired
	KemampuanRepository kemampuanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a Kemampuan
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deleteKemampuan(@PathVariable(value = "id") Long kemampuanId) {
	   	
	   	Kemampuan kemampuan = kemampuanRepository.findById(kemampuanId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	kemampuanRepository.delete(kemampuan);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get Kemampuan
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllKemampuan(){
	   	
			List<Kemampuan> listKemampuanEntity = kemampuanRepository.findAll();
			List<DTOKemampuan> listDTOKemampuan = new ArrayList<DTOKemampuan>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (Kemampuan kemampuanEntity : listKemampuanEntity) {
				DTOKemampuan kemampuanDTO = new DTOKemampuan();
				
				kemampuanDTO = modelMapper.map(kemampuanEntity, DTOKemampuan.class);
				
				listDTOKemampuan.add(kemampuanDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOKemampuan);

	   	return result;	
	   }
	   
	   // Create a new  Kemampuan
	   @PostMapping("/create")
	   public Map<String, Object> createDTOKemampuan(@Valid @RequestBody DTOKemampuan kemampuanDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       Kemampuan kemampuanEntity = new Kemampuan();

	       kemampuanEntity = modelMapper.map(kemampuanDTO, Kemampuan.class);
	       
	       kemampuanRepository.save(kemampuanEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", kemampuanDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOKemampuan(@PathVariable(value = "id") Long kemampuanId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Kemampuan kemampuanEntity = kemampuanRepository.findById(kemampuanId).get();
	   	DTOKemampuan kemampuanDTO = new DTOKemampuan();
	   	
	   	kemampuanDTO = modelMapper.map(kemampuanEntity, DTOKemampuan.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", kemampuanDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOKemampuan(@PathVariable(value = "id") Long kemampuanId,
	           @Valid @RequestBody DTOKemampuan kemampuanDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Kemampuan kemampuanEntity = kemampuanRepository.findById(kemampuanId).get();

	   	kemampuanEntity = modelMapper.map(kemampuanDTO, Kemampuan.class);

	       kemampuanRepository.save(kemampuanEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", kemampuanDTO);
	       
	   	return result;
	   }
}
