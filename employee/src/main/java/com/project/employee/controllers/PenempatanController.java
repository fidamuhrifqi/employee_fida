package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOPenempatan;
import com.project.employee.models.Penempatan;
import com.project.employee.repositories.PenempatanRepository;

@RestController
@RequestMapping("/api/penempatan")
public class PenempatanController {

	@Autowired
	PenempatanRepository penempatanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a Penempatan
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deletePenempatan(@PathVariable(value = "id") Long penempatanId) {
	   	
	   	Penempatan penempatan = penempatanRepository.findById(penempatanId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	penempatanRepository.delete(penempatan);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get Penempatan
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllPenempatan(){
	   	
			List<Penempatan> listPenempatanEntity = penempatanRepository.findAll();
			List<DTOPenempatan> listDTOPenempatan = new ArrayList<DTOPenempatan>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (Penempatan penempatanEntity : listPenempatanEntity) {
				DTOPenempatan penempatanDTO = new DTOPenempatan();
				
				penempatanDTO = modelMapper.map(penempatanEntity, DTOPenempatan.class);
				
				listDTOPenempatan.add(penempatanDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOPenempatan);

	   	return result;	
	   }
	   
	   // Create a new  Penempatan
	   @PostMapping("/create")
	   public Map<String, Object> createDTOPenempatan(@Valid @RequestBody DTOPenempatan penempatanDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       Penempatan penempatanEntity = new Penempatan();

	       penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);
	       
	       penempatanRepository.save(penempatanEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", penempatanDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOPenempatan(@PathVariable(value = "id") Long penempatanId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Penempatan penempatanEntity = penempatanRepository.findById(penempatanId).get();
	   	DTOPenempatan penempatanDTO = new DTOPenempatan();
	   	
	   	penempatanDTO = modelMapper.map(penempatanEntity, DTOPenempatan.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", penempatanDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOPenempatan(@PathVariable(value = "id") Long penempatanId,
	           @Valid @RequestBody DTOPenempatan penempatanDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Penempatan penempatanEntity = penempatanRepository.findById(penempatanId).get();

	   	penempatanEntity = modelMapper.map(penempatanDTO, Penempatan.class);

	       penempatanRepository.save(penempatanEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", penempatanDTO);
	       
	   	return result;
	   }
}
