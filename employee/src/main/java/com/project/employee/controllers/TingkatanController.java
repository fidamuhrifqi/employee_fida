package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOTingkatan;
import com.project.employee.models.Tingkatan;
import com.project.employee.repositories.TingkatanRepository;

@RestController
@RequestMapping("/api/tingkatan")
public class TingkatanController {

	@Autowired
	TingkatanRepository tingkatanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a Tingkatan
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deleteTingkatan(@PathVariable(value = "id") Long tingkatanId) {
	   	
	   	Tingkatan tingkatan = tingkatanRepository.findById(tingkatanId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	tingkatanRepository.delete(tingkatan);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get Tingkatan
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllTingkatan(){
	   	
			List<Tingkatan> listTingkatanEntity = tingkatanRepository.findAll();
			List<DTOTingkatan> listDTOTingkatan = new ArrayList<DTOTingkatan>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (Tingkatan tingkatanEntity : listTingkatanEntity) {
				DTOTingkatan tingkatanDTO = new DTOTingkatan();
				
				tingkatanDTO = modelMapper.map(tingkatanEntity, DTOTingkatan.class);
				
				listDTOTingkatan.add(tingkatanDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOTingkatan);

	   	return result;	
	   }
	   
	   // Create a new  Tingkatan
	   @PostMapping("/create")
	   public Map<String, Object> createDTOTingkatan(@Valid @RequestBody DTOTingkatan tingkatanDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       Tingkatan tingkatanEntity = new Tingkatan();

	       tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);
	       
	       tingkatanRepository.save(tingkatanEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", tingkatanDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOTingkatan(@PathVariable(value = "id") Long tingkatanId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Tingkatan tingkatanEntity = tingkatanRepository.findById(tingkatanId).get();
	   	DTOTingkatan tingkatanDTO = new DTOTingkatan();
	   	
	   	tingkatanDTO = modelMapper.map(tingkatanEntity, DTOTingkatan.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", tingkatanDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOTingkatan(@PathVariable(value = "id") Long tingkatanId,
	           @Valid @RequestBody DTOTingkatan tingkatanDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Tingkatan tingkatanEntity = tingkatanRepository.findById(tingkatanId).get();

	   	tingkatanEntity = modelMapper.map(tingkatanDTO, Tingkatan.class);

	       tingkatanRepository.save(tingkatanEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", tingkatanDTO);
	       
	   	return result;
	   }
}
