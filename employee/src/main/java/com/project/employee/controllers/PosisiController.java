package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOPosisi;
import com.project.employee.models.Posisi;
import com.project.employee.repositories.PosisiRepository;

@RestController
@RequestMapping("/api/posisi")
public class PosisiController {
	
	@Autowired
	PosisiRepository posisiRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a Posisi
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deletePosisi(@PathVariable(value = "id") Long posisiId) {
	   	
	   	Posisi posisi = posisiRepository.findById(posisiId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	posisiRepository.delete(posisi);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get Posisi
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllPosisi(){
	   	
			List<Posisi> listPosisiEntity = posisiRepository.findAll();
			List<DTOPosisi> listDTOPosisi = new ArrayList<DTOPosisi>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (Posisi posisiEntity : listPosisiEntity) {
				DTOPosisi posisiDTO = new DTOPosisi();
				
				posisiDTO = modelMapper.map(posisiEntity, DTOPosisi.class);
				
				listDTOPosisi.add(posisiDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOPosisi);

	   	return result;	
	   }
	   
	   // Create a new  Posisi
	   @PostMapping("/create")
	   public Map<String, Object> createDTOPosisi(@Valid @RequestBody DTOPosisi posisiDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       Posisi posisiEntity = new Posisi();

	       posisiEntity = modelMapper.map(posisiDTO, Posisi.class);
	       
	       posisiRepository.save(posisiEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", posisiDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOPosisi(@PathVariable(value = "id") Long posisiId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Posisi posisiEntity = posisiRepository.findById(posisiId).get();
	   	DTOPosisi posisiDTO = new DTOPosisi();
	   	
	   	posisiDTO = modelMapper.map(posisiEntity, DTOPosisi.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", posisiDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOPosisi(@PathVariable(value = "id") Long posisiId,
	           @Valid @RequestBody DTOPosisi posisiDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Posisi posisiEntity = posisiRepository.findById(posisiId).get();

	   	posisiEntity = modelMapper.map(posisiDTO, Posisi.class);

	       posisiRepository.save(posisiEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", posisiDTO);
	       
	   	return result;
	   }
}
