package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOUser;
import com.project.employee.models.User;
import com.project.employee.models.UserId;
import com.project.employee.repositories.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {
	@Autowired
	UserRepository userRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a User
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deleteUser(@PathVariable(value = "id") UserId userId) {
	   	
	   	User user = userRepository.findById(userId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	userRepository.delete(user);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get User
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllUser(){
	   	
			List<User> listUserEntity = userRepository.findAll();
			List<DTOUser> listDTOUser = new ArrayList<DTOUser>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (User userEntity : listUserEntity) {
				DTOUser userDTO = new DTOUser();
				
				userDTO = modelMapper.map(userEntity, DTOUser.class);
				
				listDTOUser.add(userDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOUser);

	   	return result;	
	   }
	   
	   // Create a new  User
	   @PostMapping("/create")
	   public Map<String, Object> createDTOUser(@Valid @RequestBody DTOUser userDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       User userEntity = new User();

	       userEntity = modelMapper.map(userDTO, User.class);
	       
	       userRepository.save(userEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", userDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOUser(@PathVariable(value = "id") UserId userId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	User userEntity = userRepository.findById(userId).get();
	   	DTOUser userDTO = new DTOUser();
	   	
	   	userDTO = modelMapper.map(userEntity, DTOUser.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", userDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOUser(@PathVariable(value = "id") UserId userId,
	           @Valid @RequestBody DTOUser userDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	User userEntity = userRepository.findById(userId).get();

	   	userEntity = modelMapper.map(userDTO, User.class);

	       userRepository.save(userEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", userDTO);
	       
	   	return result;
	   }
}
