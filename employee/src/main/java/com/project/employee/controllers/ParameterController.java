package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOParameter;
import com.project.employee.models.Parameter;
import com.project.employee.repositories.ParameterRepository;

@RestController
@RequestMapping("/api/parameter")
public class ParameterController {

	@Autowired
	ParameterRepository parameterRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a Parameter
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deleteParameter(@PathVariable(value = "id") Long parameterId) {
	   	
	   	Parameter parameter = parameterRepository.findById(parameterId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	parameterRepository.delete(parameter);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get Parameter
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllParameter(){
	   	
			List<Parameter> listParameterEntity = parameterRepository.findAll();
			List<DTOParameter> listDTOParameter = new ArrayList<DTOParameter>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (Parameter parameterEntity : listParameterEntity) {
				DTOParameter parameterDTO = new DTOParameter();
				
				parameterDTO = modelMapper.map(parameterEntity, DTOParameter.class);
				
				listDTOParameter.add(parameterDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOParameter);

	   	return result;	
	   }
	   
	   // Create a new  Parameter
	   @PostMapping("/create")
	   public Map<String, Object> createDTOParameter(@Valid @RequestBody DTOParameter parameterDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       Parameter parameterEntity = new Parameter();

	       parameterEntity = modelMapper.map(parameterDTO, Parameter.class);
	       
	       parameterRepository.save(parameterEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", parameterDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOParameter(@PathVariable(value = "id") Long parameterId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Parameter parameterEntity = parameterRepository.findById(parameterId).get();
	   	DTOParameter parameterDTO = new DTOParameter();
	   	
	   	parameterDTO = modelMapper.map(parameterEntity, DTOParameter.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", parameterDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOParameter(@PathVariable(value = "id") Long parameterId,
	           @Valid @RequestBody DTOParameter parameterDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Parameter parameterEntity = parameterRepository.findById(parameterId).get();

	   	parameterEntity = modelMapper.map(parameterDTO, Parameter.class);

	       parameterRepository.save(parameterEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", parameterDTO);
	       
	   	return result;
	   }
}
