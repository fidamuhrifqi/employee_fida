package com.project.employee.controllers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.project.employee.models.Karyawan;
import com.project.employee.models.Parameter;
import com.project.employee.models.Pendapatan;
import com.project.employee.models.PresentaseGaji;
import com.project.employee.models.TunjanganPegawai;
import com.project.employee.repositories.KaryawanRepository;
import com.project.employee.repositories.ParameterRepository;
import com.project.employee.repositories.PendapatanRepository;
import com.project.employee.repositories.PenempatanRepository;
import com.project.employee.repositories.PresentaseGajiRepository;
import com.project.employee.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api/calculation/pendapatan")
public class CalculatioPendapatanController {
	
	@Autowired
    PendapatanRepository pendapatanRepository;
	@Autowired
	KaryawanRepository karyawanRepository;
	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	@Autowired
	PenempatanRepository penempatanRepository;
	@Autowired
	ParameterRepository parameterRepository;
	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	
	@PostMapping("/update/{date}")
    public Map<String, Object> calculatioPendapatan(@PathVariable("date") String inputDate){
		
		Map<String, Object> result = new HashMap<String,Object>();
		Map<String, Object> abc = new HashMap<String,Object>();
		List<Pendapatan> listPendapatan = pendapatanRepository.findAll();
        LocalDate myDate =LocalDate.parse(inputDate);
        Date date = Date.from(myDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        boolean isInputNewData = true;
		
		if (listPendapatan.isEmpty()) {
			newPendapatan(date);
		}
		else {
			for (Pendapatan pendapatan : listPendapatan) {
				if (pendapatan.getTanggalGaji().getYear() == date.getYear() && pendapatan.getTanggalGaji().getMonth() == date.getMonth()) {
					updatePendapatan(date, pendapatan);
				
				}
				else {
					isInputNewData = false;
				}
			}
			if (!isInputNewData) {
				newPendapatan(date);
			}
		}
		
		
		result.put("Status", 200);
        result.put("message", "Get Single Data Success");
        result.put("Data", abc);
        
    	return result;
    }
	
	public void newPendapatan(Date date) {
		List<Karyawan> listKaryawan = karyawanRepository.findAll();
		
		for (Karyawan karyawan : listKaryawan) {
			Pendapatan pendapatanTemp = new Pendapatan();
			
			pendapatanTemp = inputPendapatan(karyawan, date);
			
			pendapatanRepository.save(pendapatanTemp);
		}
	}
	
	public void updatePendapatan(Date date, Pendapatan pendapatan) {
		List<Karyawan> listKaryawan = karyawanRepository.findAll();
		
		for (Karyawan karyawan : listKaryawan) {
			Pendapatan pendapatanTemp = new Pendapatan();
			if (pendapatan.getKaryawan() == karyawan) {
				pendapatanTemp = inputPendapatan(karyawan, date);
				pendapatanTemp.setIdPendapatan(pendapatan.getIdPendapatan());
			
				pendapatanRepository.save(pendapatanTemp);
			}
		}
	}
	
	public Pendapatan inputPendapatan(Karyawan karyawan, Date dates) {
		
		Pendapatan pendapatan = new Pendapatan();
		BigDecimal finalPresentaseGaji = new BigDecimal(0), umkPenempatan;
		BigDecimal setKosong = new BigDecimal(0);
		int setKosongInteger = 0;
		umkPenempatan = karyawan.getPenempatan().getUmkPenempatan();
		finalPresentaseGaji = calculationPresentaseGaji(karyawan);
		
		pendapatan.setKaryawan(karyawan);
		pendapatan.setTanggalGaji(dates);
		pendapatan.setGajiPokok(calculationGajiPokok(finalPresentaseGaji, umkPenempatan));
		pendapatan.setTunjanganKeluarga(calculationTunjanganKeluarga(pendapatan.getGajiPokok(), karyawan));
		pendapatan.setTunjanganPegawai(calculationTunjanganPegawai(karyawan));
		pendapatan.setTunjanganTransport(calculationTunjanganTransport(karyawan));
		pendapatan.setGajiKotor(calculationGajiKotor(pendapatan));
		pendapatan.setPphPerbulan(setKosong);
		pendapatan.setBpjs(calculationBPJS(pendapatan.getGajiPokok()));
		pendapatan.setGajiBersih(calculationGajiBersih(pendapatan));
		pendapatan.setLamaLembur(setKosongInteger);
		pendapatan.setUangLembur(setKosong);
		pendapatan.setVariableBonus(setKosongInteger);
		pendapatan.setUangBonus(setKosong);
		pendapatan.setTakeHomePay(calculationTakeHomePay(pendapatan));
		
		return pendapatan;	
	}
	
	public BigDecimal calculationGajiPokok(BigDecimal presentaseGaji, BigDecimal umkPenempatan) {
		
		Double gajiPokok;
		gajiPokok = presentaseGaji.doubleValue() * umkPenempatan.doubleValue();
		
		return BigDecimal.valueOf(gajiPokok);
	}
	
	public BigDecimal calculationPresentaseGaji(Karyawan karyawan) {
		
		List<PresentaseGaji> listPresentaseGaji = presentaseGajiRepository.findAll(Sort.by("masaKerja").ascending());
		BigDecimal finalPresentaseGaji = new BigDecimal(0);
		List<Integer> listMasaKerja = new ArrayList<>();
		int topLimitMasaKerja;
		
		for (PresentaseGaji presentaseGaji : listPresentaseGaji) {
			boolean isPresentaseGaji = karyawan.getPosisi() == presentaseGaji.getPosisi() && karyawan.getTingkatan().getIdTingkatan() == presentaseGaji.getIdTingkatan();
			if (isPresentaseGaji) {
				listMasaKerja.add(presentaseGaji.getMasaKerja());
			}
		}
		
		topLimitMasaKerja = 0;
            if(karyawan.getMasaKerja() <= listMasaKerja.get(0)){
            	topLimitMasaKerja = listMasaKerja.get(0);
            }else if(karyawan.getMasaKerja() <= listMasaKerja.get(1)){
            	topLimitMasaKerja = listMasaKerja.get(1);
            }else {
            	topLimitMasaKerja = listMasaKerja.get(2);
            }
            
		for (PresentaseGaji presentaseGaji : listPresentaseGaji) {
				boolean isPresentaseGaji = karyawan.getPosisi() == presentaseGaji.getPosisi() && karyawan.getTingkatan().getIdTingkatan() == presentaseGaji.getIdTingkatan() && presentaseGaji.getMasaKerja() == topLimitMasaKerja;
				if (isPresentaseGaji) {
					finalPresentaseGaji = presentaseGaji.getBesaranGaji();
				}
		}
		
		return finalPresentaseGaji;
	}

	public BigDecimal calculationTunjanganKeluarga(BigDecimal gajiPokok, Karyawan karyawan) {

		List<Parameter> listParameter = parameterRepository.findAll();
		Double tunjanganKeluarga = 0.0;
		
		if (karyawan.getStatusPernikahan() == 1) {
			for (Parameter parameter : listParameter) {
				tunjanganKeluarga = gajiPokok.doubleValue() * parameter.getTKeluarga().doubleValue();
			}
		}
		
		return BigDecimal.valueOf(tunjanganKeluarga);
	}

	public BigDecimal calculationTunjanganPegawai(Karyawan karyawan) {

		List<TunjanganPegawai> listTunjanganPegawai = tunjanganPegawaiRepository.findAll();
		BigDecimal finalTunjanganTransportasi = new BigDecimal(0);
		
		for (TunjanganPegawai tunjanganPegawai : listTunjanganPegawai) {
			boolean isTunjanganPegawai = karyawan.getPosisi() == tunjanganPegawai.getPosisi() && karyawan.getTingkatan() == tunjanganPegawai.getTingkatan();
			if (isTunjanganPegawai) {
				finalTunjanganTransportasi = tunjanganPegawai.getBesaranTujnaganPegawai();
			}
		}
		
		
		return finalTunjanganTransportasi;
	}	

	public BigDecimal calculationTunjanganTransport(Karyawan karyawan) {

		List<Parameter> listParameter = parameterRepository.findAll();
		Double tunjanganTransportasi = 0.0;
		
		for (Parameter parameter : listParameter) {
			if (karyawan.getPenempatan().getKotaPenempatan().equalsIgnoreCase("jakarta")) {
				tunjanganTransportasi = parameter.getTTransport().doubleValue();
			}
		}
		
		
		return BigDecimal.valueOf(tunjanganTransportasi);
	}	

	public BigDecimal calculationGajiKotor(Pendapatan pendapatan) {
		
		Double gajiKotor = 0.0;
		
		gajiKotor = pendapatan.getGajiPokok().doubleValue() + pendapatan.getTunjanganKeluarga().doubleValue() + pendapatan.getTunjanganPegawai().doubleValue() + pendapatan.getTunjanganTransport().doubleValue();
		
		return BigDecimal.valueOf(gajiKotor);
	}

	public BigDecimal calculationBPJS(BigDecimal gajiPokok) {

		List<Parameter> listParameter = parameterRepository.findAll();
		Double setBPJS = 0.0;
		
		for (Parameter parameter : listParameter) {
			setBPJS = gajiPokok.doubleValue() * parameter.getPBpjs().doubleValue();
		}
		
		
		return BigDecimal.valueOf(setBPJS);
	}
	
	public BigDecimal calculationGajiBersih(Pendapatan pendapatan) {
		
		Double gajiBersih = 0.0;
		
		gajiBersih = pendapatan.getGajiKotor().doubleValue() - pendapatan.getPphPerbulan().doubleValue() - pendapatan.getBpjs().doubleValue();
		
		return BigDecimal.valueOf(gajiBersih);
	}

	public BigDecimal calculationTakeHomePay(Pendapatan pendapatan) {
		
		Double takeHomePay = 0.0;
		
		takeHomePay = pendapatan.getGajiBersih().doubleValue() + pendapatan.getUangBonus().doubleValue();
		
		return BigDecimal.valueOf(takeHomePay);
	}
	
	



}
