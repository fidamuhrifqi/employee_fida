package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOPendapatan;
import com.project.employee.models.Pendapatan;
import com.project.employee.repositories.PendapatanRepository;

@RestController
@RequestMapping("/api/pendapatan")
public class PendapatanController {

	@Autowired
	PendapatanRepository pendapatanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a Pendapatan
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deletePendapatan(@PathVariable(value = "id") Long pendapatanId) {
	   	
	   	Pendapatan pendapatan = pendapatanRepository.findById(pendapatanId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	pendapatanRepository.delete(pendapatan);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get Pendapatan
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllPendapatan(){
	   	
			List<Pendapatan> listPendapatanEntity = pendapatanRepository.findAll();
			List<DTOPendapatan> listDTOPendapatan = new ArrayList<DTOPendapatan>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (Pendapatan pendapatanEntity : listPendapatanEntity) {
				DTOPendapatan pendapatanDTO = new DTOPendapatan();
				
				pendapatanDTO = modelMapper.map(pendapatanEntity, DTOPendapatan.class);
				
				listDTOPendapatan.add(pendapatanDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOPendapatan);

	   	return result;	
	   }
	   
	   // Create a new  Pendapatan
	   @PostMapping("/create")
	   public Map<String, Object> createDTOPendapatan(@Valid @RequestBody DTOPendapatan pendapatanDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       Pendapatan pendapatanEntity = new Pendapatan();

	       pendapatanEntity = modelMapper.map(pendapatanDTO, Pendapatan.class);
	       
	       pendapatanRepository.save(pendapatanEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", pendapatanDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOPendapatan(@PathVariable(value = "id") Long pendapatanId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Pendapatan pendapatanEntity = pendapatanRepository.findById(pendapatanId).get();
	   	DTOPendapatan pendapatanDTO = new DTOPendapatan();
	   	
	   	pendapatanDTO = modelMapper.map(pendapatanEntity, DTOPendapatan.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", pendapatanDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOPendapatan(@PathVariable(value = "id") Long pendapatanId,
	           @Valid @RequestBody DTOPendapatan pendapatanDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	Pendapatan pendapatanEntity = pendapatanRepository.findById(pendapatanId).get();

	   	pendapatanEntity = modelMapper.map(pendapatanDTO, Pendapatan.class);

	       pendapatanRepository.save(pendapatanEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", pendapatanDTO);
	       
	   	return result;
	   }
}
