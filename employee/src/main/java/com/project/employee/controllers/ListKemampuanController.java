package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOListKemampuan;
import com.project.employee.models.ListKemampuan;
import com.project.employee.repositories.ListKemampuanRepository;

@RestController
@RequestMapping("/api/listkemampuan")
public class ListKemampuanController {
	
	@Autowired
	ListKemampuanRepository listKemampuanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a ListKemampuan
   @DeleteMapping("/delete/{id}")
   public Map<String, Object> deleteListKemampuan(@PathVariable(value = "id") Long listKemampuanId) {
   	
   	ListKemampuan listKemampuan = listKemampuanRepository.findById(listKemampuanId).get();
   	Map<String, Object> result = new HashMap<String,Object>();
   	
   	listKemampuanRepository.delete(listKemampuan);

       result.put("Status", 200);
       result.put("message", "Delete Data Success");

   	return result;	
   }
   
   // Parsing from Entity to DTO
   // Get ListKemampuan
   @GetMapping("/readAll")
   public Map<String, Object> getReadAllListKemampuan(){
   	
		List<ListKemampuan> listListKemampuanEntity = listKemampuanRepository.findAll();
		List<DTOListKemampuan> listDTOListKemampuan = new ArrayList<DTOListKemampuan>();
		Map<String, Object> result = new HashMap<String,Object>();
		
		for (ListKemampuan listKemampuanEntity : listListKemampuanEntity) {
			DTOListKemampuan listKemampuanDTO = new DTOListKemampuan();
			
			listKemampuanDTO = modelMapper.map(listKemampuanEntity, DTOListKemampuan.class);
			
			listDTOListKemampuan.add(listKemampuanDTO);
		}
		
       result.put("Status", 200);
       result.put("message", "Read All Data Success");
       result.put("Data", listDTOListKemampuan);

   	return result;	
   }
   
   // Create a new  ListKemampuan
   @PostMapping("/create")
   public Map<String, Object> createDTOListKemampuan(@Valid @RequestBody DTOListKemampuan listKemampuanDTO){
      
   	Map<String, Object> result = new HashMap<String,Object>();
       ListKemampuan listKemampuanEntity = new ListKemampuan();

       listKemampuanEntity = modelMapper.map(listKemampuanDTO, ListKemampuan.class);
       
       listKemampuanRepository.save(listKemampuanEntity);
       
       result.put("Status", 200);
       result.put("message", "Create Data Success");
       result.put("Data", listKemampuanDTO);

       return result;
   }
   
   // Get a Single Note
   @GetMapping("/getsingle/{id}")
   public Map<String, Object> getReadOneDTOListKemampuan(@PathVariable(value = "id") Long listKemampuanId){
		
   	Map<String, Object> result = new HashMap<String,Object>();
   	ListKemampuan listKemampuanEntity = listKemampuanRepository.findById(listKemampuanId).get();
   	DTOListKemampuan listKemampuanDTO = new DTOListKemampuan();
   	
   	listKemampuanDTO = modelMapper.map(listKemampuanEntity, DTOListKemampuan.class);
   	
   	result.put("Status", 200);
       result.put("message", "Get Single Data Success");
       result.put("Data", listKemampuanDTO);

       return result;
   }
   
   // Update a Note
   @PutMapping("/update/{id}")
   public Map<String, Object> updateDTOListKemampuan(@PathVariable(value = "id") Long listKemampuanId,
           @Valid @RequestBody DTOListKemampuan listKemampuanDTO) {
			
   	Map<String, Object> result = new HashMap<String,Object>();
   	ListKemampuan listKemampuanEntity = listKemampuanRepository.findById(listKemampuanId).get();

   	listKemampuanEntity = modelMapper.map(listKemampuanDTO, ListKemampuan.class);

       listKemampuanRepository.save(listKemampuanEntity);
   	
   	result.put("Status", 200);
       result.put("message", "Update Data Success");
       result.put("Data", listKemampuanDTO);
       
   	return result;
   }
}
