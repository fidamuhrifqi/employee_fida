package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOKaryawan;
import com.project.employee.models.Karyawan;
import com.project.employee.repositories.KaryawanRepository;

@RestController
@RequestMapping("/api/karyawan")
public class KaryawanController {

	@Autowired
    KaryawanRepository karyawanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a Karyawan
    @DeleteMapping("/delete/{id}")
    public Map<String, Object> deleteKaryawan(@PathVariable(value = "id") Long karyawanId) {
    	
    	Karyawan karyawan = karyawanRepository.findById(karyawanId).get();
    	Map<String, Object> result = new HashMap<String,Object>();
    	
    	karyawanRepository.delete(karyawan);

        result.put("Status", 200);
        result.put("message", "Delete Data Success");

    	return result;	
    }
    
    // Parsing from Entity to DTO
    // Get Karyawan
    @GetMapping("/readAll")
    public Map<String, Object> getReadAllKaryawan(){
    	
		List<Karyawan> listKaryawanEntity = karyawanRepository.findAll();
		List<DTOKaryawan> listDTOKaryawan = new ArrayList<DTOKaryawan>();
		Map<String, Object> result = new HashMap<String,Object>();
		
		for (Karyawan karyawanEntity : listKaryawanEntity) {
			DTOKaryawan karyawanDTO = new DTOKaryawan();
			
			karyawanDTO = modelMapper.map(karyawanEntity, DTOKaryawan.class);
			
			listDTOKaryawan.add(karyawanDTO);
		}
		
        result.put("Status", 200);
        result.put("message", "Read All Data Success");
        result.put("Data", listDTOKaryawan);

    	return result;	
    }
    
    // Create a new  Karyawan
    @PostMapping("/create")
    public Map<String, Object> createDTOKaryawan(@Valid @RequestBody DTOKaryawan karyawanDTO){
       
    	Map<String, Object> result = new HashMap<String,Object>();
        Karyawan karyawanEntity = new Karyawan();

        karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);
        
        karyawanRepository.save(karyawanEntity);
        
        result.put("Status", 200);
        result.put("message", "Create Data Success");
        result.put("Data", karyawanDTO);

        return result;
    }
    
    // Get a Single Note
    @GetMapping("/getsingle/{id}")
    public Map<String, Object> getReadOneDTOKaryawan(@PathVariable(value = "id") Long karyawanId){
		
    	Map<String, Object> result = new HashMap<String,Object>();
    	Karyawan karyawanEntity = karyawanRepository.findById(karyawanId).get();
    	DTOKaryawan karyawanDTO = new DTOKaryawan();
    	
    	karyawanDTO = modelMapper.map(karyawanEntity, DTOKaryawan.class);
    	
    	result.put("Status", 200);
        result.put("message", "Get Single Data Success");
        result.put("Data", karyawanDTO);

        return result;
    }
    
    // Update a Note
    @PutMapping("/update/{id}")
    public Map<String, Object> updateDTOKaryawan(@PathVariable(value = "id") Long karyawanId,
            @Valid @RequestBody DTOKaryawan karyawanDTO) {
			
    	Map<String, Object> result = new HashMap<String,Object>();
    	Karyawan karyawanEntity = karyawanRepository.findById(karyawanId).get();

    	karyawanEntity = modelMapper.map(karyawanDTO, Karyawan.class);

        karyawanRepository.save(karyawanEntity);
    	
    	result.put("Status", 200);
        result.put("message", "Update Data Success");
        result.put("Data", karyawanDTO);
        
    	return result;
    }
}
