package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOKategoriKemampuan;
import com.project.employee.models.KategoriKemampuan;
import com.project.employee.repositories.KategoriKemampuanRepository;

@RestController
@RequestMapping("/api/kategorikemampuan")
public class KategoriKemampuanController {

	@Autowired
	KategoriKemampuanRepository kategoriKemampuanRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a KategoriKemampuan
   @DeleteMapping("/delete/{id}")
   public Map<String, Object> deleteKategoriKemampuan(@PathVariable(value = "id") Long kategoriKemampuanId) {
   	
   	KategoriKemampuan kategoriKemampuan = kategoriKemampuanRepository.findById(kategoriKemampuanId).get();
   	Map<String, Object> result = new HashMap<String,Object>();
   	
   	kategoriKemampuanRepository.delete(kategoriKemampuan);

       result.put("Status", 200);
       result.put("message", "Delete Data Success");

   	return result;	
   }
   
   // Parsing from Entity to DTO
   // Get KategoriKemampuan
   @GetMapping("/readAll")
   public Map<String, Object> getReadAllKategoriKemampuan(){
   	
		List<KategoriKemampuan> listKategoriKemampuanEntity = kategoriKemampuanRepository.findAll();
		List<DTOKategoriKemampuan> listDTOKategoriKemampuan = new ArrayList<DTOKategoriKemampuan>();
		Map<String, Object> result = new HashMap<String,Object>();
		
		for (KategoriKemampuan kategoriKemampuanEntity : listKategoriKemampuanEntity) {
			DTOKategoriKemampuan kategoriKemampuanDTO = new DTOKategoriKemampuan();
			
			kategoriKemampuanDTO = modelMapper.map(kategoriKemampuanEntity, DTOKategoriKemampuan.class);
			
			listDTOKategoriKemampuan.add(kategoriKemampuanDTO);
		}
		
       result.put("Status", 200);
       result.put("message", "Read All Data Success");
       result.put("Data", listDTOKategoriKemampuan);

   	return result;	
   }
   
   // Create a new  KategoriKemampuan
   @PostMapping("/create")
   public Map<String, Object> createDTOKategoriKemampuan(@Valid @RequestBody DTOKategoriKemampuan kategoriKemampuanDTO){
      
   	Map<String, Object> result = new HashMap<String,Object>();
       KategoriKemampuan kategoriKemampuanEntity = new KategoriKemampuan();

       kategoriKemampuanEntity = modelMapper.map(kategoriKemampuanDTO, KategoriKemampuan.class);
       
       kategoriKemampuanRepository.save(kategoriKemampuanEntity);
       
       result.put("Status", 200);
       result.put("message", "Create Data Success");
       result.put("Data", kategoriKemampuanDTO);

       return result;
   }
   
   // Get a Single Note
   @GetMapping("/getsingle/{id}")
   public Map<String, Object> getReadOneDTOKategoriKemampuan(@PathVariable(value = "id") Long kategoriKemampuanId){
		
   	Map<String, Object> result = new HashMap<String,Object>();
   	KategoriKemampuan kategoriKemampuanEntity = kategoriKemampuanRepository.findById(kategoriKemampuanId).get();
   	DTOKategoriKemampuan kategoriKemampuanDTO = new DTOKategoriKemampuan();
   	
   	kategoriKemampuanDTO = modelMapper.map(kategoriKemampuanEntity, DTOKategoriKemampuan.class);
   	
   	result.put("Status", 200);
       result.put("message", "Get Single Data Success");
       result.put("Data", kategoriKemampuanDTO);

       return result;
   }
   
   // Update a Note
   @PutMapping("/update/{id}")
   public Map<String, Object> updateDTOKategoriKemampuan(@PathVariable(value = "id") Long kategoriKemampuanId,
           @Valid @RequestBody DTOKategoriKemampuan kategoriKemampuanDTO) {
			
   	Map<String, Object> result = new HashMap<String,Object>();
   	KategoriKemampuan kategoriKemampuanEntity = kategoriKemampuanRepository.findById(kategoriKemampuanId).get();

   	kategoriKemampuanEntity = modelMapper.map(kategoriKemampuanDTO, KategoriKemampuan.class);

       kategoriKemampuanRepository.save(kategoriKemampuanEntity);
   	
   	result.put("Status", 200);
       result.put("message", "Update Data Success");
       result.put("Data", kategoriKemampuanDTO);
       
   	return result;
   }
}
