package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOPresentaseGaji;
import com.project.employee.models.PresentaseGaji;
import com.project.employee.repositories.PresentaseGajiRepository;

@RestController
@RequestMapping("/api/presentaseGaji")
public class PresentaseGajiController {

	@Autowired
	PresentaseGajiRepository presentaseGajiRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a PresentaseGaji
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deletePresentaseGaji(@PathVariable(value = "id") Long presentaseGajiId) {
	   	
	   	PresentaseGaji presentaseGaji = presentaseGajiRepository.findById(presentaseGajiId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	presentaseGajiRepository.delete(presentaseGaji);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get PresentaseGaji
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllPresentaseGaji(){
	   	
			List<PresentaseGaji> listPresentaseGajiEntity = presentaseGajiRepository.findAll();
			List<DTOPresentaseGaji> listDTOPresentaseGaji = new ArrayList<DTOPresentaseGaji>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (PresentaseGaji presentaseGajiEntity : listPresentaseGajiEntity) {
				DTOPresentaseGaji presentaseGajiDTO = new DTOPresentaseGaji();
				
				presentaseGajiDTO = modelMapper.map(presentaseGajiEntity, DTOPresentaseGaji.class);
				
				listDTOPresentaseGaji.add(presentaseGajiDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOPresentaseGaji);

	   	return result;	
	   }
	   
	   // Create a new  PresentaseGaji
	   @PostMapping("/create")
	   public Map<String, Object> createDTOPresentaseGaji(@Valid @RequestBody DTOPresentaseGaji presentaseGajiDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       PresentaseGaji presentaseGajiEntity = new PresentaseGaji();

	       presentaseGajiEntity = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);
	       
	       presentaseGajiRepository.save(presentaseGajiEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", presentaseGajiDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOPresentaseGaji(@PathVariable(value = "id") Long presentaseGajiId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	PresentaseGaji presentaseGajiEntity = presentaseGajiRepository.findById(presentaseGajiId).get();
	   	DTOPresentaseGaji presentaseGajiDTO = new DTOPresentaseGaji();
	   	
	   	presentaseGajiDTO = modelMapper.map(presentaseGajiEntity, DTOPresentaseGaji.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", presentaseGajiDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOPresentaseGaji(@PathVariable(value = "id") Long presentaseGajiId,
	           @Valid @RequestBody DTOPresentaseGaji presentaseGajiDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	PresentaseGaji presentaseGajiEntity = presentaseGajiRepository.findById(presentaseGajiId).get();

	   	presentaseGajiEntity = modelMapper.map(presentaseGajiDTO, PresentaseGaji.class);

	       presentaseGajiRepository.save(presentaseGajiEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", presentaseGajiDTO);
	       
	   	return result;
	   }
}
