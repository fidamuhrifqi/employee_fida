package com.project.employee.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.employee.DTOmodels.DTOTunjanganPegawai;
import com.project.employee.models.TunjanganPegawai;
import com.project.employee.repositories.TunjanganPegawaiRepository;

@RestController
@RequestMapping("/api/tunjanganpegawai")
public class TunjanganPegawaiController {

	@Autowired
	TunjanganPegawaiRepository tunjanganPegawaiRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	 // Delete a TunjanganPegawai
	   @DeleteMapping("/delete/{id}")
	   public Map<String, Object> deleteTunjanganPegawai(@PathVariable(value = "id") Long tunjanganPegawaiId) {
	   	
	   	TunjanganPegawai tunjanganPegawai = tunjanganPegawaiRepository.findById(tunjanganPegawaiId).get();
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	
	   	tunjanganPegawaiRepository.delete(tunjanganPegawai);

	       result.put("Status", 200);
	       result.put("message", "Delete Data Success");

	   	return result;	
	   }
	   
	   // Parsing from Entity to DTO
	   // Get TunjanganPegawai
	   @GetMapping("/readAll")
	   public Map<String, Object> getReadAllTunjanganPegawai(){
	   	
			List<TunjanganPegawai> listTunjanganPegawaiEntity = tunjanganPegawaiRepository.findAll();
			List<DTOTunjanganPegawai> listDTOTunjanganPegawai = new ArrayList<DTOTunjanganPegawai>();
			Map<String, Object> result = new HashMap<String,Object>();
			
			for (TunjanganPegawai tunjanganPegawaiEntity : listTunjanganPegawaiEntity) {
				DTOTunjanganPegawai tunjanganPegawaiDTO = new DTOTunjanganPegawai();
				
				tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawaiEntity, DTOTunjanganPegawai.class);
				
				listDTOTunjanganPegawai.add(tunjanganPegawaiDTO);
			}
			
	       result.put("Status", 200);
	       result.put("message", "Read All Data Success");
	       result.put("Data", listDTOTunjanganPegawai);

	   	return result;	
	   }
	   
	   // Create a new  TunjanganPegawai
	   @PostMapping("/create")
	   public Map<String, Object> createDTOTunjanganPegawai(@Valid @RequestBody DTOTunjanganPegawai tunjanganPegawaiDTO){
	      
	   	Map<String, Object> result = new HashMap<String,Object>();
	       TunjanganPegawai tunjanganPegawaiEntity = new TunjanganPegawai();

	       tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);
	       
	       tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);
	       
	       result.put("Status", 200);
	       result.put("message", "Create Data Success");
	       result.put("Data", tunjanganPegawaiDTO);

	       return result;
	   }
	   
	   // Get a Single Note
	   @GetMapping("/getsingle/{id}")
	   public Map<String, Object> getReadOneDTOTunjanganPegawai(@PathVariable(value = "id") Long tunjanganPegawaiId){
			
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(tunjanganPegawaiId).get();
	   	DTOTunjanganPegawai tunjanganPegawaiDTO = new DTOTunjanganPegawai();
	   	
	   	tunjanganPegawaiDTO = modelMapper.map(tunjanganPegawaiEntity, DTOTunjanganPegawai.class);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Get Single Data Success");
	       result.put("Data", tunjanganPegawaiDTO);

	       return result;
	   }
	   
	   // Update a Note
	   @PutMapping("/update/{id}")
	   public Map<String, Object> updateDTOTunjanganPegawai(@PathVariable(value = "id") Long tunjanganPegawaiId,
	           @Valid @RequestBody DTOTunjanganPegawai tunjanganPegawaiDTO) {
				
	   	Map<String, Object> result = new HashMap<String,Object>();
	   	TunjanganPegawai tunjanganPegawaiEntity = tunjanganPegawaiRepository.findById(tunjanganPegawaiId).get();

	   	tunjanganPegawaiEntity = modelMapper.map(tunjanganPegawaiDTO, TunjanganPegawai.class);

	       tunjanganPegawaiRepository.save(tunjanganPegawaiEntity);
	   	
	   	result.put("Status", 200);
	       result.put("message", "Update Data Success");
	       result.put("Data", tunjanganPegawaiDTO);
	       
	   	return result;
	   }
}
