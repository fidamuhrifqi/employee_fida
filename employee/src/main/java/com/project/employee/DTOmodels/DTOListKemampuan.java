package com.project.employee.DTOmodels;

public class DTOListKemampuan {

	private long idListKemampuan;
	private DTOKaryawan karyawan;
	private DTOKemampuan kemampuan;
	private Long nilaiKemampuan;
	
	public long getIdListKemampuan() {
		return idListKemampuan;
	}
	public void setIdListKemampuan(long idListKemampuan) {
		this.idListKemampuan = idListKemampuan;
	}
	public DTOKaryawan getKaryawan() {
		return karyawan;
	}
	public void setKaryawan(DTOKaryawan karyawan) {
		this.karyawan = karyawan;
	}
	public DTOKemampuan getKemampuan() {
		return kemampuan;
	}
	public void setKemampuan(DTOKemampuan kemampuan) {
		this.kemampuan = kemampuan;
	}
	public Long getNilaiKemampuan() {
		return nilaiKemampuan;
	}
	public void setNilaiKemampuan(Long nilaiKemampuan) {
		this.nilaiKemampuan = nilaiKemampuan;
	}
	
	
}
