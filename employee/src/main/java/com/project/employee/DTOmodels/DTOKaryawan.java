package com.project.employee.DTOmodels;

import java.util.Date;


public class DTOKaryawan {

	private long idKaryawan;
	private DTOAgama agama;
	private DTOPenempatan penempatan;
	private DTOPosisi posisi;
	private DTOTingkatan tingkatan;
	private String nama;
	private String noKtp;
	private String alamat;
	private Date tanggalLahir;
	private Integer masaKerja;
	private Short statusPernikahan;
	private Date kontrakAwal;
	private Date kontrakAkhir;
	private String jenisKelamin;
	private Integer jumlahAnak;
	
	public long getIdKaryawan() {
		return idKaryawan;
	}
	public void setIdKaryawan(long idKaryawan) {
		this.idKaryawan = idKaryawan;
	}
	public DTOAgama getAgama() {
		return agama;
	}
	public void setAgama(DTOAgama agama) {
		this.agama = agama;
	}
	public DTOPenempatan getPenempatan() {
		return penempatan;
	}
	public void setPenempatan(DTOPenempatan penempatan) {
		this.penempatan = penempatan;
	}
	public DTOPosisi getPosisi() {
		return posisi;
	}
	public void setPosisi(DTOPosisi posisi) {
		this.posisi = posisi;
	}
	public DTOTingkatan getTingkatan() {
		return tingkatan;
	}
	public void setTingkatan(DTOTingkatan tingkatan) {
		this.tingkatan = tingkatan;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNoKtp() {
		return noKtp;
	}
	public void setNoKtp(String noKtp) {
		this.noKtp = noKtp;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public Date getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public Integer getMasaKerja() {
		return masaKerja;
	}
	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}
	public Short getStatusPernikahan() {
		return statusPernikahan;
	}
	public void setStatusPernikahan(Short statusPernikahan) {
		this.statusPernikahan = statusPernikahan;
	}
	public Date getKontrakAwal() {
		return kontrakAwal;
	}
	public void setKontrakAwal(Date kontrakAwal) {
		this.kontrakAwal = kontrakAwal;
	}
	public Date getKontrakAkhir() {
		return kontrakAkhir;
	}
	public void setKontrakAkhir(Date kontrakAkhir) {
		this.kontrakAkhir = kontrakAkhir;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public Integer getJumlahAnak() {
		return jumlahAnak;
	}
	public void setJumlahAnak(Integer jumlahAnak) {
		this.jumlahAnak = jumlahAnak;
	}
}
