package com.project.employee.DTOmodels;

public class DTOPosisi {

	private long idPosisi;
	private String namaPosisi;
	
	public long getIdPosisi() {
		return idPosisi;
	}
	public void setIdPosisi(long idPosisi) {
		this.idPosisi = idPosisi;
	}
	public String getNamaPosisi() {
		return namaPosisi;
	}
	public void setNamaPosisi(String namaPosisi) {
		this.namaPosisi = namaPosisi;
	}
	
	
}
