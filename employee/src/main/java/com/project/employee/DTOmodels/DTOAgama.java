package com.project.employee.DTOmodels;

public class DTOAgama {
	
	private long idAgama;
	private String namaAgama;
	
	public long getIdAgama() {
		return idAgama;
	}
	public void setIdAgama(long idAgama) {
		this.idAgama = idAgama;
	}
	public String getNamaAgama() {
		return namaAgama;
	}
	public void setNamaAgama(String namaAgama) {
		this.namaAgama = namaAgama;
	}
	
	
}
