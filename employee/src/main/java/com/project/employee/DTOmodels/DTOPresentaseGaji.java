package com.project.employee.DTOmodels;

import java.math.BigDecimal;

public class DTOPresentaseGaji {

	private long idPresentaseGaji;
	private DTOPosisi posisi;
	private Long idTingkatan;
	private BigDecimal besaranGaji;
	private Integer masaKerja;
	
	public long getIdPresentaseGaji() {
		return idPresentaseGaji;
	}
	public void setIdPresentaseGaji(long idPresentaseGaji) {
		this.idPresentaseGaji = idPresentaseGaji;
	}
	public DTOPosisi getPosisi() {
		return posisi;
	}
	public void setPosisi(DTOPosisi posisi) {
		this.posisi = posisi;
	}
	public Long getIdTingkatan() {
		return idTingkatan;
	}
	public void setIdTingkatan(Long idTingkatan) {
		this.idTingkatan = idTingkatan;
	}
	public BigDecimal getBesaranGaji() {
		return besaranGaji;
	}
	public void setBesaranGaji(BigDecimal besaranGaji) {
		this.besaranGaji = besaranGaji;
	}
	public Integer getMasaKerja() {
		return masaKerja;
	}
	public void setMasaKerja(Integer masaKerja) {
		this.masaKerja = masaKerja;
	}
}
