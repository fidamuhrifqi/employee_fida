package com.project.employee.DTOmodels;

import java.math.BigDecimal;

public class DTOTunjanganPegawai {

	private long idTunjanganPegawai;
	private DTOPosisi posisi;
	private DTOTingkatan tingkatan;
	private BigDecimal besaranTujnaganPegawai;
	
	public long getIdTunjanganPegawai() {
		return idTunjanganPegawai;
	}
	public void setIdTunjanganPegawai(long idTunjanganPegawai) {
		this.idTunjanganPegawai = idTunjanganPegawai;
	}
	public DTOPosisi getPosisi() {
		return posisi;
	}
	public void setPosisi(DTOPosisi posisi) {
		this.posisi = posisi;
	}
	public DTOTingkatan getTingkatan() {
		return tingkatan;
	}
	public void setTingkatan(DTOTingkatan tingkatan) {
		this.tingkatan = tingkatan;
	}
	public BigDecimal getBesaranTujnaganPegawai() {
		return besaranTujnaganPegawai;
	}
	public void setBesaranTujnaganPegawai(BigDecimal besaranTujnaganPegawai) {
		this.besaranTujnaganPegawai = besaranTujnaganPegawai;
	}
	
	
}
