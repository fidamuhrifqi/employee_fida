package com.project.employee.DTOmodels;


public class DTOUser {

	private DTOUserId id;
	private String password;
	private Short status;
	
	public DTOUserId getId() {
		return id;
	}
	public void setId(DTOUserId id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Short getStatus() {
		return status;
	}
	public void setStatus(Short status) {
		this.status = status;
	}
	
	
	
}
