package com.project.employee.DTOmodels;



public class DTOKemampuan {

	private long idKemampuan;
	private DTOKategoriKemampuan kategoriKemampuan;
	private String namaKemampuan;
	
	public long getIdKemampuan() {
		return idKemampuan;
	}
	public void setIdKemampuan(long idKemampuan) {
		this.idKemampuan = idKemampuan;
	}
	public DTOKategoriKemampuan getKategoriKemampuan() {
		return kategoriKemampuan;
	}
	public void setKategoriKemampuan(DTOKategoriKemampuan kategoriKemampuan) {
		this.kategoriKemampuan = kategoriKemampuan;
	}
	public String getNamaKemampuan() {
		return namaKemampuan;
	}
	public void setNamaKemampuan(String namaKemampuan) {
		this.namaKemampuan = namaKemampuan;
	}
	
	
}