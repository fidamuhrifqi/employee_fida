package com.project.employee.DTOmodels;

public class DTOKategoriKemampuan {

	private long idKategori;
	private String namaKategori;
	
	public long getIdKategori() {
		return idKategori;
	}
	public void setIdKategori(long idKategori) {
		this.idKategori = idKategori;
	}
	public String getNamaKategori() {
		return namaKategori;
	}
	public void setNamaKategori(String namaKategori) {
		this.namaKategori = namaKategori;
	}
	
	
}
