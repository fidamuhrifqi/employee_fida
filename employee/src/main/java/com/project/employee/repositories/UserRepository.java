package com.project.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.project.employee.models.User;
import com.project.employee.models.UserId;

@Repository
public interface UserRepository extends JpaRepository<User, UserId>{

}