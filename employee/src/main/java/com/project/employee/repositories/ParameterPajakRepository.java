package com.project.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.project.employee.models.ParameterPajak;

@Repository
public interface ParameterPajakRepository extends JpaRepository<ParameterPajak, Long>{

}