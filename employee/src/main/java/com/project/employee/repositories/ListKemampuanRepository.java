package com.project.employee.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.project.employee.models.ListKemampuan;

@Repository
public interface ListKemampuanRepository extends JpaRepository<ListKemampuan, Long>{

}